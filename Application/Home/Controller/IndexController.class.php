<?php
namespace Home\Controller;
use Common\Common\Controller;
class IndexController extends Controller {
	public function _empty(){
		$this->index();
	}
    public function index(){
        $this->display('index');
    }
    
    public function needajax(){
    	//$par = var_export(I('param.'),true);
    	//$this->log->log('無效的訪問請求',$par);
    	$this->display('index');
    }
    
}
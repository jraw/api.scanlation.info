<?php
/**
 * Created by PhpStorm.
 * User: dr490n
 * Date: 2015/8/22
 * Time: 17:48
 */

namespace Home\Controller;
use Common\Common\Controller;

class TestController extends Controller
{
    public function _empty(){
        redirect('/Home/Index?url='.$this->url,0);
    }

    public function picupload($type='post')
    {
        header('Access-Control-Allow-Origin:*');
        $array = S('testPicPostArray');
        $url = U('Test/pictest');
        if(empty($array)) $array = array();
        switch($type)
        {
            case 'cecikwan':
                $key = $type.time();
                $array[] = $key;
                $input = I('post.');
                $input['_time'] = date('Y-m-d H:i:s');
                S("testPicPostType{$key}",$input,3600*24);
                $url = U('Test/pictest',"type={$type}&key={$key}");
                break;
            case 'post':
                $key = $type.time();
                $array[] = $key;
                $input = I('post.');
                $input['_time'] = date('Y-m-d H:i:s');
                S("testPicPostType{$key}",$input,3600*24);
                $url = U('Test/pictest',"type={$type}&key={$key}");
                break;
            default:
                break;
        }
        S('testPicPostArray',$array,3600*24);
        $ret = array('status'=>200,'desc'=>'提交成功','url'=>$url);
        $this->ajaxReturn($ret,'JSON');
    }

    public function pictest($type='post',$key=false)
    {
        if(false === $key)
        {
            $array = S('testPicPostArray');
            echo '<ol>';
            foreach($array as $item)
            {
                echo '<li>';
                echo '<a href="'.U('Test/pictest',"type={$type}&key={$item}").'">';
                echo $item.'</a><hr /></li>';
            }
            return;
        }else {
            $input =  S("testPicPostType{$key}");
            switch ($type) {
                case 'cecikwan':
                    echo $input['_time'];
                    echo '<hr/>';
                    echo "<p>USER:{$input['user']}</p>";
                    echo "<p>TOKEN:{$input['token']}</p>";
                    echo "<p>TITLE:{$input['title']}</p>";
                    echo "<p>OFFSET:{$input['offset']}</p>";
                    echo '<p>PICLIST:</p><hr/>';
                    //var_dump($input['data']);
                    $data = json_decode(htmlspecialchars_decode($input['data']),true);
                    echo "<ol>";
                    foreach($data as $item)
                    {
                        echo '<li>';
                        echo "<p>URL:{$item['url']}</p>";
                        echo "<p>WIDTH:{$item['width']}</p>";
                        echo "<p>HEIGHT:{$item['height']}</p>";
                        echo '</li>';
                    }
                    break;
                default:
                    var_dump($input);
            }
        }
    }
}
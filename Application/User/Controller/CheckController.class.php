<?php
/**
 * Created by PhpStorm.
 * User: dr490n
 * Date: 2015/8/29
 * Time: 20:57
 */

namespace User\Controller;
use Common\Common\Controller;
class CheckController extends Controller
{
    public function _empty(){
        redirect('/Home/Index?url='.$this->url,0);
    }

    public function menu($app)
    {
        $callback = I('get.callback',false);
        $is_jsonp = !empty($callback);

        $data = array('desc'=>'用户未授权！');

        $this->doReturn(403,$data,$is_jsonp);
    }
}
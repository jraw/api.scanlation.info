<?php
/**
 * Created by PhpStorm.
 * User: dr490n
 * Date: 2015/8/20
 * Time: 20:51
 */

namespace Tieba\Controller;

use Common\Common\Controller;
class MangaHotListController extends Controller
{
    public function _empty(){
        redirect('/Home/Index?url='.$this->url,0);
    }

    private $retField = 'postid, title, url, posttype, forumname, author, id';
    private $typeArr = array(1,6,8);

    public function all()
    {
        /*
         if(!IS_AJAX)
         {
         $par = var_export(I('param.'),true);
         $this->log->log('无效的访问请求',$par);
         redirect('/Home/Index/needajax?url='.$this->url,0);
         return;
         }
         // */
        $callback = I('get.callback',false);
        $is_jsonp = !empty($callback);
        $page = intval(I('param.page',1));
        $size = intval(I('param.size',30));
        if($page < 1) $page = 1;
        $from = ($page-1)*$size;

        $db = M('viewtopclick');
        $where = array(
            'posttype' => array('in',$this->typeArr),
            'checked' => 3
        );
        $tsname = 'clickMangaList';
        $count = S("{$tsname}Count");
        if(empty($count))
        {
            $count = $db->where($where)->cache("{$tsname}Count",600)->count();
        }

        if($from > $count)
        {
            $data = array(
                'desc' => '已經顯示到最後一行！',
            );
            $this->doReturn(494,$data,$is_jsonp);
        }

        $list = S("{$tsname}Page{$from}");
        if(empty($list))
        {
            $list = $db->where($where)->order('`sum` desc')
                ->field($this->retField)
                ->limit($size)->page($page)
                ->cache("{$tsname}Page{$from}",600)->select();
        }

        $itemCount = count($list);
        $next = ($from+$itemCount == $count)?false:($page+1);

        $data = array(
            'totalCount' => $count,
            'itemCount'=>$itemCount,
            'data' => $list,
            'desc' => 'OK',
            'next' =>$next
        );

        $this->doReturn(200,$data,$is_jsonp);
    }

    public function today()
    {
        /*
         if(!IS_AJAX)
         {
         $par = var_export(I('param.'),true);
         $this->log->log('无效的访问请求',$par);
         redirect('/Home/Index/needajax?url='.$this->url,0);
         return;
         }
         // */
        $callback = I('get.callback',false);
        $is_jsonp = !empty($callback);
        $page = intval(I('param.page',1));
        $size = intval(I('param.size',30));
        if($page < 1) $page = 1;
        $from = ($page-1)*$size;

        $db = M('viewclicktoday');
        $where = array(
            'posttype' => array('in',$this->typeArr),
            'checked' => 3
        );
        $tsname = 'clickMangaListToday';
        $count = S("{$tsname}Count");
        if(empty($count))
        {
            $count = $db->join("`postlist` ON `viewClickToday`.`postid`=`postlist`.`postid`")
                ->where($where)->cache("{$tsname}Count",600)->count();
        }

        if($from > $count)
        {
            $data = array(
                'desc' => '已經顯示到最後一行！',
            );
            $this->doReturn(494,$data,$is_jsonp);
        }

        $list = S("{$tsname}Page{$from}");
        if(empty($list))
        {
            $list = $db->join("`postlist` ON `viewClickToday`.`postid`=`postlist`.`postid`")
                ->where($where)->order('`sum` desc')
                ->field('`postlist`.'.$this->retField)
                ->limit($size)->page($page)
                ->cache("{$tsname}Page{$from}",600)->select();
        }

        $itemCount = count($list);
        $next = ($from+$itemCount == $count)?false:($page+1);

        $data = array(
            'totalCount' => $count,
            'itemCount'=>$itemCount,
            'data' => $list,
            'desc' => 'OK',
            'next' =>$next
        );

        $this->doReturn(200,$data,$is_jsonp);
    }

    public function week()
    {
        /*
         if(!IS_AJAX)
         {
         $par = var_export(I('param.'),true);
         $this->log->log('无效的访问请求',$par);
         redirect('/Home/Index/needajax?url='.$this->url,0);
         return;
         }
         // */
        $callback = I('get.callback',false);
        $is_jsonp = !empty($callback);
        $page = intval(I('param.page',1));
        $size = intval(I('param.size',30));
        if($page < 1) $page = 1;
        $from = ($page-1)*$size;

        $db = M('viewclickweek');
        $where = array(
            'posttype' => array('in',$this->typeArr),
            'checked' => 3
        );
        $tsname = 'clickMangaListWeek';
        $count = S("{$tsname}Count");
        if(empty($count))
        {
            $count = $db->join("`postlist` ON `viewClickWeek`.`postid`=`postlist`.`postid`")
                ->where($where)->cache("{$tsname}Count",600)->count();
        }

        if($from > $count)
        {
            $data = array(
                'desc' => '已經顯示到最後一行！',
            );
            $this->doReturn(494,$data,$is_jsonp);
        }

        $list = S("{$tsname}Page{$from}");
        if(empty($list))
        {
            $list = $db->join("`postlist` ON `viewClickWeek`.`postid`=`postlist`.`postid`")
                ->where($where)->order('`sum` desc')
                ->field('`postlist`.'.$this->retField)
                ->limit($size)->page($page)
                ->cache("{$tsname}Page{$from}",600)->select();
        }

        $itemCount = count($list);
        $next = ($from+$itemCount == $count)?false:($page+1);

        $data = array(
            'totalCount' => $count,
            'itemCount'=>$itemCount,
            'data' => $list,
            'desc' => 'OK',
            'next' =>$next
        );

        $this->doReturn(200,$data,$is_jsonp);
    }

    public function month()
    {
        /*
         if(!IS_AJAX)
         {
         $par = var_export(I('param.'),true);
         $this->log->log('无效的访问请求',$par);
         redirect('/Home/Index/needajax?url='.$this->url,0);
         return;
         }
         // */
        $callback = I('get.callback',false);
        $is_jsonp = !empty($callback);
        $page = intval(I('param.page',1));
        $size = intval(I('param.size',30));
        if($page < 1) $page = 1;
        $from = ($page-1)*$size;

        $db = M('viewclickmonth');
        $where = array(
            'posttype' => array('in',$this->typeArr),
            'checked' => 3
        );
        $tsname = 'clickMangaListMonth';
        $count = S("{$tsname}Count");
        if(empty($count))
        {
            $count = $db->join("`postlist` ON `viewClickMonth`.`postid`=`postlist`.`postid`")
                ->where($where)->cache("{$tsname}Count",600)->count();
        }

        if($from > $count)
        {
            $data = array(
                'desc' => '已經顯示到最後一行！',
            );
            $this->doReturn(494,$data,$is_jsonp);
        }

        $list = S("{$tsname}Page{$from}");
        if(empty($list))
        {
            $list = $db->join("`postlist` ON `viewClickMonth`.`postid`=`postlist`.`postid`")
                ->where($where)->order('`sum` desc')
                ->field('`postlist`.'.$this->retField)
                ->limit($size)->page($page)
                ->cache("{$tsname}Page{$from}",600)->select();
        }

        $itemCount = count($list);
        $next = ($from+$itemCount == $count)?false:($page+1);

        $data = array(
            'totalCount' => $count,
            'itemCount'=>$itemCount,
            'data' => $list,
            'desc' => 'OK',
            'next' =>$next
        );

        $this->doReturn(200,$data,$is_jsonp);
    }

}
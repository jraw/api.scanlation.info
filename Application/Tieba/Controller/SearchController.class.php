<?php
namespace Tieba\Controller;
use Common\Common\Controller;
use Common\Common\TradChineseConv;
class SearchController extends Controller {
	public function _empty(){
		redirect('/Home/Index?url='.$this->url,0);
	}
	
	private $retField = 'title, url, postid, posttype, checked, createdate, forumname, author';
	
	public function all()
	{
		/*
		if(!IS_AJAX)
		{
			$par = var_export(I('param.'),true);
			$this->log->log('无效的访问请求',$par);
			redirect('/Home/Index/needajax?url='.$this->url,0);
			return;
		}
		// */
		$callback = I('get.callback',false);
		$is_jsonp = !empty($callback);
		$retWord = $word = I('param.word',false);
		$page = intval(I('param.page',1));
		$size = intval(I('param.size',30));
		$order = I('param.order',false);
		if($page < 1) $page = 1;
		$from = ($page-1)*$size;
		if(empty($word))
		{
			$data = array(
					'desc' => '請輸入搜索詞！',
			);
			$this->doReturn(403,$data,$is_jsonp);
		}
		
		if(false !== strpos($word,'%'))
		{
			$word = str_replace('%','\%',$word);
		}
		
		$word = TradChineseConv::simplified($word);
		if($retWord === $word)
		{
			$word = TradChineseConv::traditional($word);
		}
		
		$db = M('postlist');
		$count = $db->where("(`title` LIKE '%%%s%%' OR `title` LIKE '%%%s%%') AND `posttype` >= 0",$word,$retWord)->count();
		
		if(0 == $count)
		{
			$data = array(
					'desc' => '未查到相關條目！',
			);
			$this->doReturn(404,$data,$is_jsonp);
		}
		
		if($from > $count)
		{
			$data = array(
					'desc' => '已經顯示到最後一行！',
			);
			$this->doReturn(494,$data,$is_jsonp);
		}
		
		$item = $db->where("(`title` LIKE '%%%s%%' OR `title` LIKE '%%%s%%') AND `posttype` >= 0",$word,$retWord)
					->field($this->retField)
					->limit($size)->page($page)
					->order('`posttype` desc, `postid` '.($order?'ASC':'DESC'))
					->select();
		$itemCount = count($item);
		$next = ($from+$itemCount == $count)?false:($page+1);
		
		$slid = $this->searchlog($retWord,$count);
		
		$data = array(
				'word' => $retWord,
				'totalCount' => $count,
				'itemCount'=>$itemCount,
				'data' => $item,
				'desc' => 'OK',
				'next' =>$next,
				'logid' => $slid
		);
		
		$this->doReturn(200,$data,$is_jsonp);
		
	}
	
	public function group($type)
	{
		/*
		 if(!IS_AJAX)
		 {
		 $par = var_export(I('param.'),true);
		 $this->log->log('无效的访问请求',$par);
		 redirect('/Home/Index/needajax?url='.$this->url,0);
		 return;
		 }
		 // */
		
		$callback = I('get.callback',false);
		$is_jsonp = !empty($callback);
		
		$typeArr = array(-1,0,1,6,8,10,100);
		
		if(!in_array($type,$typeArr))
		{
			$data = array(
					'desc' => '指定的貼子類型無效！',
			);
			$this->doReturn(403,$data,$is_jsonp);
		}
		
		$retWord = $word = I('param.word',false);
		$page = intval(I('param.page',1));
		$size = intval(I('param.size',30));
		$order = I('param.order',false);
		if($page < 1) $page = 1;
		$from = ($page-1)*$size;
		
		$db = M('postlist');
		
		if(empty($word))
		{
			$count = $db->where("`posttype`=%d",$type)->count();
			
			if(0 == $count)
			{
				$data = array(
						'desc' => '未查到相關條目！',
				);
				$this->doReturn(404,$data,$is_jsonp);
			}
			
			if($from > $count)
			{
				$data = array(
						'desc' => '已經顯示到最後一行！',
				);
				$this->doReturn(494,$data,$is_jsonp);
			}
			
			$item = $db->where("`posttype`=%d",$type)
			->field($this->retField)
			->limit($size)->page($page)
			->order('`posttype` desc,`postid` '.($order?'ASC':'DESC'))
			->select();
			$itemCount = count($item);
			$next = ($from+$itemCount == $count)?false:($page+1);
			
			$data = array(
					'word' => $retWord,
					'totalCount' => $count,
					'itemCount'=>$itemCount,
					'data' => $item,
					'desc' => 'OK',
					'next' =>$next
			);
			
			$this->doReturn(200,$data,$is_jsonp);
		}
		else
		{
			if(false !== strpos($word,'%'))
			{
				$word = str_replace('%','\%',$word);
			}

			$word = TradChineseConv::simplified($word);
			if($retWord === $word)
			{
				$word = TradChineseConv::traditional($word);
			}

			$count = $db->where("(`title` LIKE '%%%s%%' OR `title` LIKE '%%%s%%') AND `posttype`=%d", $word, $retWord, $type)->count();
			
			if(0 == $count)
			{
				$data = array(
						'desc' => '未查到相關條目！',
				);
				$this->doReturn(404,$data,$is_jsonp);
			}
			
			if($from > $count)
			{
				$data = array(
						'desc' => '已經顯示到最後一行！',
				);
				$this->doReturn(494,$data,$is_jsonp);
			}
			
			$item = $db->where("(`title` LIKE '%%%s%%' OR `title` LIKE '%%%s%%') AND `posttype`=%d", $word, $retWord, $type)
			->field($this->retField)
			->limit($size)->page($page)
			->order('postid '.($order?'ASC':'DESC'))
			->select();
			$itemCount = count($item);
			$next = ($from+$itemCount == $count)?false:($page+1);
			
			$slid = $this->searchlog($retWord,$count,$type);
			
			$data = array(
					'word' => $retWord,
					'totalCount' => $count,
					'itemCount'=>$itemCount,
					'data' => $item,
					'desc' => 'OK',
					'next' =>$next,
					'logid' => $slid
			);
			
			$this->doReturn(200,$data,$is_jsonp);
		}
	}
	
	public function log()
	{
		/*
		 if(!IS_AJAX)
		 {
		 $par = var_export(I('param.'),true);
		 $this->log->log('无效的访问请求',$par);
		 redirect('/Home/Index/needajax?url='.$this->url,0);
		 return;
		 }
		 // */
		
		$callback = I('get.callback',false);
		$is_jsonp = !empty($callback);
		
		$id = intval(I('param.id',false));
		if(empty($id))
		{
			$data = array(
					'desc' => '無效的搜索編號！',
			);
			$this->doReturn(403,$data,$is_jsonp);
		}
		
		$log = M('searchlog');
		$data = $log->where("`id`=%d",$id)->field('id,posttype,word')->find();
		$this->doReturn(200,$data,$is_jsonp);
	}
	
	public function count()
	{
		/*
			if(!IS_AJAX)
			{
			$par = var_export(I('param.'),true);
			$this->log->log('无效的访问请求',$par);
			redirect('/Home/Index/needajax?url='.$this->url,0);
			return;
			}
			// */
	
		$callback = I('get.callback',false);
		$is_jsonp = !empty($callback);
		
		$cacheData = S('Tieba_countCache');
		if(!empty($cacheData))
		{
			$this->doReturn(200,array('data'=>$cacheData,'desc'=>'CACHE'),$is_jsonp);
		}
		
		$db = M('postlist');
		
		$count = $db->count();
		
		$item = $db->query("select count(*) as `count`,`posttype` FROM `postlist` WHERE `posttype`>=0 GROUP BY `posttype`");
		
		//dump($item);
		
		//echo '<br />'.$count;
		$data = array();
		$data[] = array(
			'type' => '數據總數',
			'posttype' => 'ALL',
			'count' => $count
		);
		
		foreach ($item as $v)
		{
			switch($v['posttype'])
			{
				case 1:
					$v['type'] = '漫畫貼';
					$data[] = $v;
					break;
				case 6:
					$v['type'] = '漢化貼';
					$data[] = $v;
					break;
				case 8:
					$v['type'] = '短篇貼';
					$data[] = $v;
					break;
				case 10:
					$v['type'] = '整合貼';
					$data[] = $v;
					break;
				case 100:
					$v['type'] = '下載貼';
					$data[] = $v;
					break;
				default:

					$v['type'] = '未知類型'.$v['posttype'];
					$data[] = $v;
					break;
			}
		}
		
		S('Tieba_countCache',$data,600);
		
		$this->doReturn(200,array('data'=>$data,'desc'=>'OK'),$is_jsonp);
		
	}
	
	private function searchlog($word,$total,$type=null)
	{
		if(empty($word))return;
		
		$searchSession = I('session.searchhistory',array());
		$wordMD5 = md5($word.$total.((null === $type)?'all':$type));
		if(!in_array($wordMD5,$searchSession))
		{
			$searchSession[] = $wordMD5;
			$_SESSION['viewhistory'] = $searchSession;
		}else{
			return;
		}
		
		$date = date('Y-m-d 00:00:00');
		$log = M('searchlog');
		
		$map['posttype'] = (null === $type)?array('exp',' IS NULL'):$type;
		
		$slid = $log->where("`word`='%s' AND `createdate` > '%s'",$word,$date)
					->where($map)->getField('id');
		if(empty($slid))
		{
			$data = array(
				'word' => $word,
				'posttype' => $type,
				'totalline' => $total,
				'updatedate' => date('Y-m-d H:i:s'),
				'click' => 1
			);
			$slid = $log->data($data)->add();
		}else{
			$data = array(
					'id' => $slid,
					'totalline' => $total,
					'updatedate' => date('Y-m-d H:i:s')
			);
			$log->where("id=%d",$slid)->setInc('click');
			$log->save($data);
		}
		return $slid;
	}

}
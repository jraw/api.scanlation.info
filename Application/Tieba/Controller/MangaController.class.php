<?php
namespace Tieba\Controller;
use Common\Common\Controller;
class MangaController extends Controller {
    public function _empty(){
        redirect('/Home/Index?url='.$this->url,0);
    }

    private $retField = 'title, url, postid, posttype, forumname, author, id';
    private $picField = 'imgcode, dbid, width, height, mime, pindex, postid';

    public function info($id=null,$pid=null)
    {
        $callback = I('get.callback',false);
        $is_jsonp = !empty($callback);
        $id = isset($id)?$id:I('get.id',null);
        $pid = isset($pid)?$pid:I('get.pid',null);

        $db = M('postlist');
        $where = array();
        if(isset($id))
        {
            $where['id'] = $id;
        }
        else if(isset($pid))
        {
            $where['postid'] = $pid;
        }else{
            $data = array(
                'desc' => '未查到匹配的貼子信息！',
            );
            $this->doReturn(404,$data,$is_jsonp);
        }

        $item = $db->where($where)->field($this->retField)->find();

        $data = array(
            'data' => $item,
            'desc' => 'OK'
        );

        $this->doReturn(200,$data,$is_jsonp);
    }

    public function pic($id=null,$pid=null)
    {
        $callback = I('get.callback',false);
        $is_jsonp = !empty($callback);

        $db = M('postimg');
        $where = array();

        if(isset($id))
        {
            $where['dbid'] = $id;
        }
        else if(isset($pid))
        {
            $where['postid'] = $pid;
        }else{
            $data = array(
                'desc' => '未查到匹配的貼子信息！',
            );
            $this->doReturn(404,$data,$is_jsonp);
        }

        $list = $db->where($where)->field($this->picField)->order('`pindex` asc')->select();

        $itemCount = count($list);

        $data = array(
            'count'=>$itemCount,
            'data' => $list,
            'desc' => 'OK'
        );

        $this->doReturn(200,$data,$is_jsonp);
    }

    public function view()
    {
        $callback = I('get.callback',false);
        $is_jsonp = !empty($callback);
        $pid = I('param.pid',false);
        if(empty($pid))
        {
            $data = array('desc'=>'Empty Post ID!');
            $this->doReturn(404,$data,$is_jsonp);
        }
        $this->viewlog($pid);
        $data = array('desc'=>'OK');
        $this->doReturn(200,$data,$is_jsonp);
    }

    private function viewlog($post)
    {
        if(empty($post))return;

        $viewSession = I('session.viewhistory',array());
        if(!in_array($post,$viewSession))
        {
            $viewSession[] = $post;
            $_SESSION['viewhistory'] = $viewSession;
        }else{
            return;
        }

        $date = date('Y-m-d 00:00:00');
        $log = M('viewlog');
        $vlid = $log->where('`postid`="'.$post.'" AND `createdate` > "'.$date.'"')->getField('id');
        if(empty($vlid))
        {
            $data = array(
                'postid' => $post,
                'updatedate' => date('Y-m-d H:i:s'),
                'click' => 1
            );
            $log->data($data)->add();
        }else{
            $data = array(
                'id' => $vlid,
                'updatedate' => date('Y-m-d H:i:s')
            );
            $log->where('id='.$vlid)->setInc('click');
            $log->save($data);
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: dr490n
 * Date: 2015/8/28
 * Time: 23:26
 */

namespace Tieba\Controller;
use Common\Common\Controller;

class MangaListController extends Controller
{
    public function _empty()
    {
        redirect('/Home/Index?url=' . $this->url, 0);
    }

    private $retField = 'title, url, postid, posttype, forumname, author, id';
    private $typeArr = array(1,6,8);

    public function update()
    {
        /*
         if(!IS_AJAX)
         {
         $par = var_export(I('param.'),true);
         $this->log->log('无效的访问请求',$par);
         redirect('/Home/Index/needajax?url='.$this->url,0);
         return;
         }
         // */
        $callback = I('get.callback',false);
        $is_jsonp = !empty($callback);
        $page = intval(I('param.page',1));
        $size = intval(I('param.size',30));
        if($page < 1) $page = 1;
        $from = ($page-1)*$size;

        $db = M('postlist');
        $where = array(
            'posttype' => array('in',$this->typeArr),
            'checked' => 3
        );
        $count = S('updateMangaListCount');
        if(empty($count))
        {
            $count = $db->where($where)->cache('updateMangaListCount',600)->count();
        }

        if($from > $count)
        {
            $data = array(
                'desc' => '已經顯示到最後一行！',
            );
            $this->doReturn(494,$data,$is_jsonp);
        }

        $list = S("updateMangaListPage{$from}");
        if(empty($list))
        {
            $list = $db->where($where)->order('`postid` desc')
                ->field($this->retField)
                ->limit($size)->page($page)
                ->cache("updateMangaListPage{$from}",600)->select();
        }

        $itemCount = count($list);
        $next = ($from+$itemCount == $count)?false:($page+1);

        $data = array(
            'totalCount' => $count,
            'itemCount'=>$itemCount,
            'data' => $list,
            'desc' => 'OK',
            'next' =>$next
        );

        $this->doReturn(200,$data,$is_jsonp);
    }

    public function group($type)
    {
        /*
         if(!IS_AJAX)
         {
         $par = var_export(I('param.'),true);
         $this->log->log('无效的访问请求',$par);
         redirect('/Home/Index/needajax?url='.$this->url,0);
         return;
         }
         // */
        $callback = I('get.callback',false);
        $is_jsonp = !empty($callback);


        if(!in_array($type,$this->typeArr))
        {
            $data = array(
                'desc' => '指定的貼子類型無效！',
            );
            $this->doReturn(403,$data,$is_jsonp);
        }

        $page = intval(I('param.page',1));
        $size = intval(I('param.size',30));
        if($page < 1) $page = 1;
        $from = ($page-1)*$size;

        $db = M('postlist');
        $count = S("updateMangaGroup{$type}Count");

        if(empty($count))
        {
            $count = $db->where("`posttype`=%d AND `checked` = 3",$type)->cache("updateMangaGroup{$type}Count",600)->count();
        }

        if($from > $count)
        {
            $data = array(
                'desc' => '已經顯示到最後一行！',
            );
            $this->doReturn(494,$data,$is_jsonp);
        }

        $list = S("updateMangaGroup{$type}From{$from}");
        if(empty($list))
        {
            $list = $db->where("`posttype` = %d AND `checked` = 3",$type)->order('`postid` desc')
                ->field($this->retField)
                ->limit($size)->page($page)
                ->cache("updateMangaGroup{$type}From{$from}",600)->select();
        }

        $itemCount = count($list);
        $next = ($from+$itemCount == $count)?false:($page+1);

        $data = array(
            'totalCount' => $count,
            'itemCount'=>$itemCount,
            'data' => $list,
            'desc' => 'OK',
            'next' =>$next
        );

        $this->doReturn(200,$data,$is_jsonp);
    }
}
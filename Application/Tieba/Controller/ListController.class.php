<?php
namespace Tieba\Controller;
use Common\Common\Controller;
class ListController extends Controller {
	public function _empty(){
		redirect('/Home/Index?url='.$this->url,0);
	}
	
	private $retField = 'title, url, postid, posttype, checked, createdate, forumname, author, id';
	
	public function update()
	{
		/*
		 if(!IS_AJAX)
		 {
		 $par = var_export(I('param.'),true);
		 $this->log->log('无效的访问请求',$par);
		 redirect('/Home/Index/needajax?url='.$this->url,0);
		 return;
		 }
		 // */
		$callback = I('get.callback',false);
		$is_jsonp = !empty($callback);
		$page = intval(I('param.page',1));
		$size = intval(I('param.size',30));
		if($page < 1) $page = 1;
		$from = ($page-1)*$size;
		
		$db = M('postlist');
		$count = S('updateListCount');
		if(empty($count))
		{
			$count = $db->where("`posttype` >= 0")->cache('updateListCount',600)->count();
		}
		
		if($from > $count)
		{
			$data = array(
					'desc' => '已經顯示到最後一行！',
			);
			$this->doReturn(494,$data,$is_jsonp);
		}
		
		$list = S("updateListPage{$from}");
		if(empty($list))
		{
			$list = $db->where("`posttype` >= 0")->order('`postid` desc')
						->field($this->retField)
						->limit($size)->page($page)
						->cache("updateListPage{$from}",600)->select();
		}
		
		$itemCount = count($list);
		$next = ($from+$itemCount == $count)?false:($page+1);
		
		$data = array(
				'totalCount' => $count,
				'itemCount'=>$itemCount,
				'data' => $list,
				'desc' => 'OK',
				'next' =>$next
		);
			
		$this->doReturn(200,$data,$is_jsonp);
	}
	
	public function group($type)
	{
		/*
		 if(!IS_AJAX)
		 {
		 $par = var_export(I('param.'),true);
		 $this->log->log('无效的访问请求',$par);
		 redirect('/Home/Index/needajax?url='.$this->url,0);
		 return;
		 }
		 // */
		$callback = I('get.callback',false);
		$is_jsonp = !empty($callback);
		
		$typeArr = array(-1,0,1,6,8,10,100);
		
		if(!in_array($type,$typeArr))
		{
			$data = array(
					'desc' => '指定的貼子類型無效！',
			);
			$this->doReturn(403,$data,$is_jsonp);
		}
		
		$page = intval(I('param.page',1));
		$size = intval(I('param.size',30));
		if($page < 1) $page = 1;
		$from = ($page-1)*$size;
		
		$db = M('postlist');
		$count = S("updateGroup{$type}Count");
		
		if(empty($count))
		{
			$count = $db->where("`posttype`=%d AND `checked` >= 0",$type)->cache("updateGroup{$type}Count",600)->count();
		}
		
		if($from > $count)
		{
			$data = array(
					'desc' => '已經顯示到最後一行！',
			);
			$this->doReturn(494,$data,$is_jsonp);
		}
		
		$list = S("updateGroup{$type}From{$from}");
		if(empty($list))
		{
			$list = $db->where("`posttype` = %d AND `checked` >= 0",$type)->order('`postid` desc')
						->field($this->retField)
						->limit($size)->page($page)
						->cache("updateGroup{$type}From{$from}",600)->select();
		}
		
		$itemCount = count($list);
		$next = ($from+$itemCount == $count)?false:($page+1);
		
		$data = array(
				'totalCount' => $count,
				'itemCount'=>$itemCount,
				'data' => $list,
				'desc' => 'OK',
				'next' =>$next
		);
			
		$this->doReturn(200,$data,$is_jsonp);
	}
	
	public function date($type='all',$year=false,$month=false,$day=false)
	{
		/*
		 if(!IS_AJAX)
		 {
		 $par = var_export(I('param.'),true);
		 $this->log->log('无效的访问请求',$par);
		 redirect('/Home/Index/needajax?url='.$this->url,0);
		 return;
		 }
		 // */
		$callback = I('get.callback',false);
		$is_jsonp = !empty($callback);
		
		$typeArr = array('all',-1,0,1,6,8,10,100);
		
		if(!in_array(strtolower($type),$typeArr))
		{
			$data = array(
					'desc' => '指定的貼子類型無效！',
			);
			$this->doReturn(403,$data,$is_jsonp);
		}
		
		$dArray = getdate();
		
		if(empty($day) && empty($month) && empty($year))
		{
			$datefrom = date('Y-m-d 00:00:00');
			$dateto = date('Y-m-d 23:59:59');
		}
		else if(empty($day) && empty($month) && !empty($year))
		{
			if($year < 1000) $year += 1900;
			$datefrom = date('Y-m-d H:i:s',mktime(0,0,0,1,1,$year));
			$dateto = date('Y-m-d H:i:s',mktime(0,0,0,1,1,$year+1));
		}
		else if(empty($year) && empty($day) && !empty($month))
		{
			$year = $dArray['year'];
			$datefrom = date('Y-m-d H:i:s',mktime(0,0,0,$month,1,$year));
			$dateto = date('Y-m-d H:i:s',mktime(0,0,0,$month+1,1,$year));
		}else if(empty($year) && empty($month) && !empty($day))
		{
			$month = $dArray['mon'];
			$datefrom = date('Y-m-d H:i:s',mktime(0,0,0,$month,$day,$year));
			$dateto = date('Y-m-d H:i:s',mktime(0,0,0,$month,$day+1,$year));
		}else if(empty($day) && !empty($month) && !empty($year))
		{
			$datefrom = date('Y-m-d H:i:s',mktime(0,0,0,$month,1,$year));
			$dateto = date('Y-m-d H:i:s',mktime(0,0,0,$month+1,1,$year));
		}
		else if(empty($year) && !empty($month) && !empty($day))
		{
			$year = $dArray['year'];
			$datefrom = date('Y-m-d H:i:s',mktime(0,0,0,$month,$day,$year));
			$dateto = date('Y-m-d H:i:s',mktime(0,0,0,$month,$day+1,$year));
		}else if(!empty($year) && !empty($month) && !empty($day))
		{
			$datefrom = date('Y-m-d H:i:s',mktime(0,0,0,$month,$day,$year));
			$dateto = date('Y-m-d H:i:s',mktime(0,0,0,$month,$day+1,$year));
		}
		else
		{
			$data = array(
					'desc' => '無效的時間格式！',
			);
			$this->doReturn(403,$data,$is_jsonp);
		}
		
		if(date_create($datefrom) > date_create())
		{
			$data = array(
					'desc' => '無效的時間起點！',
			);
			$this->doReturn(403,$data,$is_jsonp);
		}
		if(date_create($dateto) > date_create())
		{
			$dateto = date('Y-m-d H:i:s');
		}
		
		$page = intval(I('param.page',1));
		$size = intval(I('param.size',30));
		if($page < 1) $page = 1;
		$from = ($page-1)*$size;
		
		$db = M('postlist');
		$count = S("archive{$type}CountF{$datefrom}T{$dateto}");
		
		if(empty($count))
		{
			$map = array(
				'posttype' => ('all' === $type)?array('egt',0):$type,
				'checked' => array('egt',0),
				'createdate' => array('between',array($datefrom,$dateto))
			);
			$count = $db->where($map)->cache("archive{$type}CountF{$datefrom}T{$dateto}",600)->count();
		}
		
		if($from > $count)
		{
			$data = array(
					'desc' => '已經顯示到最後一行！',
			);
			$this->doReturn(494,$data,$is_jsonp);
		}
		
		$list = S("archive{$type}F{$datefrom}T{$dateto}From{$from}");
		if(empty($list))
		{
			$map = array(
					'posttype' => ('all' === $type)?array('egt',0):$type,
					'checked' => array('egt',0),
					'createdate' => array('between',array($datefrom,$dateto))
			);
			$list = $db->where($map)->order('`postid` desc')
			->field($this->retField)
			->limit($size)->page($page)
			->cache("archive{$type}F{$datefrom}T{$dateto}From{$from}",600)->select();
		}
		
		$itemCount = count($list);
		$next = ($from+$itemCount == $count)?false:($page+1);
		
		$data = array(
				'datefrom' => $datefrom,
				'dateto' => $dateto,
				'totalCount' => $count,
				'itemCount'=>$itemCount,
				'data' => $list,
				'desc' => 'OK',
				'next' =>$next
		);
			
		$this->doReturn(200,$data,$is_jsonp);
	}
}
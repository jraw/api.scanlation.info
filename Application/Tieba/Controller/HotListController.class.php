<?php
/**
 * Created by PhpStorm.
 * User: dr490n
 * Date: 2015/8/20
 * Time: 20:51
 */

namespace Tieba\Controller;

use Common\Common\Controller;
class HotListController extends Controller
{
    public function _empty(){
        redirect('/Home/Index?url='.$this->url,0);
    }

    private $retField = 'postid, title, url, posttype, forumname, author,id';

    public function all()
    {
        /*
         if(!IS_AJAX)
         {
         $par = var_export(I('param.'),true);
         $this->log->log('无效的访问请求',$par);
         redirect('/Home/Index/needajax?url='.$this->url,0);
         return;
         }
         // */
        $callback = I('get.callback',false);
        $is_jsonp = !empty($callback);
        $page = intval(I('param.page',1));
        $size = intval(I('param.size',30));
        if($page < 1) $page = 1;
        $from = ($page-1)*$size;

        $db = M('viewtopclick');
        $count = S('clickListCount');
        if(empty($count))
        {
            $count = $db->where("`posttype` >= 0")->cache('clickListCount',600)->count();
        }

        if($from > $count)
        {
            $data = array(
                'desc' => '已經顯示到最後一行！',
            );
            $this->doReturn(494,$data,$is_jsonp);
        }

        $list = S("clickListPage{$from}");
        if(empty($list))
        {
            $list = $db->where("`posttype` >= 0")->order('`sum` desc')
                ->field($this->retField)
                ->limit($size)->page($page)
                ->cache("clickListPage{$from}",600)->select();
        }

        $itemCount = count($list);
        $next = ($from+$itemCount == $count)?false:($page+1);

        $data = array(
            'totalCount' => $count,
            'itemCount'=>$itemCount,
            'data' => $list,
            'desc' => 'OK',
            'next' =>$next
        );

        $this->doReturn(200,$data,$is_jsonp);
    }

    public function today()
    {
        /*
         if(!IS_AJAX)
         {
         $par = var_export(I('param.'),true);
         $this->log->log('无效的访问请求',$par);
         redirect('/Home/Index/needajax?url='.$this->url,0);
         return;
         }
         // */
        $callback = I('get.callback',false);
        $is_jsonp = !empty($callback);
        $page = intval(I('param.page',1));
        $size = intval(I('param.size',30));
        if($page < 1) $page = 1;
        $from = ($page-1)*$size;

        $db = M('viewclicktoday');
        $count = S('clickListCountToday');
        if(empty($count))
        {
            $count = $db->join("`postlist` ON `viewClickToday`.`postid`=`postlist`.`postid`")
                ->where("`posttype` >= 0")->cache('clickListCountToday',600)->count();
        }

        if($from > $count)
        {
            $data = array(
                'desc' => '已經顯示到最後一行！',
            );
            $this->doReturn(494,$data,$is_jsonp);
        }

        $list = S("clickListTodayPage{$from}");
        if(empty($list))
        {
            $list = $db->join("`postlist` ON `viewClickToday`.`postid`=`postlist`.`postid`")
                ->where("`posttype` >= 0")->order('`sum` desc')
                ->field('`postlist`.'.$this->retField)
                ->limit($size)->page($page)
                ->cache("clickListTodayPage{$from}",600)->select();
        }

        $itemCount = count($list);
        $next = ($from+$itemCount == $count)?false:($page+1);

        $data = array(
            'totalCount' => $count,
            'itemCount'=>$itemCount,
            'data' => $list,
            'desc' => 'OK',
            'next' =>$next
        );

        $this->doReturn(200,$data,$is_jsonp);
    }

    public function week()
    {
        /*
         if(!IS_AJAX)
         {
         $par = var_export(I('param.'),true);
         $this->log->log('无效的访问请求',$par);
         redirect('/Home/Index/needajax?url='.$this->url,0);
         return;
         }
         // */
        $callback = I('get.callback',false);
        $is_jsonp = !empty($callback);
        $page = intval(I('param.page',1));
        $size = intval(I('param.size',30));
        if($page < 1) $page = 1;
        $from = ($page-1)*$size;

        $db = M('viewclickweek');
        $count = S('clickListCountWeek');
        if(empty($count))
        {
            $count = $db->join("`postlist` ON `viewClickWeek`.`postid`=`postlist`.`postid`")
                ->where("`posttype` >= 0")->cache('clickListCountWeek',600)->count();
        }

        if($from > $count)
        {
            $data = array(
                'desc' => '已經顯示到最後一行！',
            );
            $this->doReturn(494,$data,$is_jsonp);
        }

        $list = S("clickListWeekPage{$from}");
        if(empty($list))
        {
            $list = $db->join("`postlist` ON `viewClickWeek`.`postid`=`postlist`.`postid`")
                ->where("`posttype` >= 0")->order('`sum` desc')
                ->field('`postlist`.'.$this->retField)
                ->limit($size)->page($page)
                ->cache("clickListWeekPage{$from}",600)->select();
        }

        $itemCount = count($list);
        $next = ($from+$itemCount == $count)?false:($page+1);

        $data = array(
            'totalCount' => $count,
            'itemCount'=>$itemCount,
            'data' => $list,
            'desc' => 'OK',
            'next' =>$next
        );

        $this->doReturn(200,$data,$is_jsonp);
    }

    public function month()
    {
        /*
         if(!IS_AJAX)
         {
         $par = var_export(I('param.'),true);
         $this->log->log('无效的访问请求',$par);
         redirect('/Home/Index/needajax?url='.$this->url,0);
         return;
         }
         // */
        $callback = I('get.callback',false);
        $is_jsonp = !empty($callback);
        $page = intval(I('param.page',1));
        $size = intval(I('param.size',30));
        if($page < 1) $page = 1;
        $from = ($page-1)*$size;

        $db = M('viewclickmonth');
        $count = S('clickListCountMonth');
        if(empty($count))
        {
            $count = $db->join("`postlist` ON `viewClickMonth`.`postid`=`postlist`.`postid`")
                ->where("`posttype` >= 0")->cache('clickListCountMonth',600)->count();
        }

        if($from > $count)
        {
            $data = array(
                'desc' => '已經顯示到最後一行！',
            );
            $this->doReturn(494,$data,$is_jsonp);
        }

        $list = S("clickListMonthPage{$from}");
        if(empty($list))
        {
            $list = $db->join("`postlist` ON `viewClickMonth`.`postid`=`postlist`.`postid`")
                ->where("`posttype` >= 0")->order('`sum` desc')
                ->field('`postlist`.'.$this->retField)
                ->limit($size)->page($page)
                ->cache("clickListMonthPage{$from}",600)->select();
        }

        $itemCount = count($list);
        $next = ($from+$itemCount == $count)?false:($page+1);

        $data = array(
            'totalCount' => $count,
            'itemCount'=>$itemCount,
            'data' => $list,
            'desc' => 'OK',
            'next' =>$next
        );

        $this->doReturn(200,$data,$is_jsonp);
    }

}
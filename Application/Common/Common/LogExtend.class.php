<?php
namespace Common\Common;

class LogExtend
{
	private $session_id = false;
	private $url = false;
	private $source = 'LOGSYSTEM';

	public function __construct($sid,$url,$source=null)
	{
		$this->session_id = $sid;
		$this->url = $url;
		if(isset($source)) $this->source = $source;
	}

	public function error($desc,$info,$source=false)
	{
		$this->log($desc, $info, $source, 99);
	}

	public function fatal($desc,$info,$source=false)
	{
		$this->log($desc, $info, $source, 999);
	}

	public function debug($desc,$info,$source=false)
	{
		$this->log($desc, $info, $source, -1);
	}

	public function log($desc,$info,$source=false,$type=0)
	{
		$log = M('Log',null,'BASE_DB_CFG');
		if(empty($source))
		{
			$source = $this->source;
		}
		$data = array(
				'log_type' => $type,
				'source'=> $source,
				'session_id' => $this->session_id,
				'url' => $this->url,
				'title' => $desc,
				'log' => $info
		);
		$log->data($data)->add();
	}
}
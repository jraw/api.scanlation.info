<?php
namespace Common\Common;
class Controller  extends \Think\Controller
{
	protected $userinfo = false;
	protected $session_id = false;
	protected $sessionGUID = false;
	protected $log = false;
	protected $url = false;

	public function __construct()
	{
		parent::__construct();
		// 全站用同一个GUID标记用户过程
		$gus_guid = I('param.gus_guid',null);
		if(isset($_SESSION['sessionGUID']))
		{
			$this->sessionGUID = $_SESSION['sessionGUID'];
		}
		else if(!empty($gus_guid))
		{
			$this->sessionGUID = $gus_guid;
		}
		else
		{
			$this->sessionGUID = UtilityFunctions::guid(true);
		}
		$this->session_id = session_id();
		$this->url = ('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		$this->log = new LogExtend($this->sessionGUID,$this->url,MODULE_NAME.'/'.CONTROLLER_NAME);
		if(isset($_SESSION['userinfo']))
		{
			$this->userinfo = $_SESSION['userinfo'];
			$uid = $this->userinfo['id'];
			$this->addSession($uid);
		}else{
			$this->addSession();
		}

		$_SESSION['sessionGUID'] = $this->sessionGUID;
	}
	
	protected function doReturn($status,$data,$jsonp)
	{
		$ret = array_merge($data,array('status'=>$status,'time'=>time(),'version'=>C('VERSION')));
		$this->ajaxReturn($ret,$jsonp?'jsonp':'json');
	}

	private function addSession($uid = -1)
	{
		$sid = $this->session_id;
		$site = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:$this->url;
		$ip = $_SERVER['REMOTE_ADDR'];
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			$list = explode(',',$_SERVER['HTTP_X_FORWARDED_FOR']);
			$ip .= '->'.$list[0];
		}
		$session = M('session',null,'BASE_DB_CFG');
		$sCount = $session->where("session_id='{$sid}' AND user_id={$uid}")->Count();
		$text = "用戶IP:{$ip}\r\n";
		if(isset($_SERVER['HTTP_REFERER']))
		{
			$text .= '用戶來源:'.$_SERVER['HTTP_REFERER']."\r\n";
		}
		$text .= '用戶提交的數據:'.var_export(I('param.'),true);
		if($sCount > 0)
		{
			$data['update_time'] = date('Y-m-d H:i:s');
			//$data['user_id'] = $uid;
			$session->where("session_id='{$sid}' AND user_id={$uid}")->save($data);
			if($uid === -1)
			{
				$this->log->log("遊客{$ip}訪問",$text,'visit');
			}else{
				$this->log->log("用戶({$uid})訪問",$text,'visit');
			}
		}else{
			$data['session_id'] = $sid;
			$data['session_guid'] = UtilityFunctions::guid();
			$data['update_time'] = date('Y-m-d H:i:s');
			$data['user_id'] = $uid;
			$data['from_site'] = $site;
			$data['user_info'] = $_SERVER['HTTP_USER_AGENT'];
			$data['user_ip'] = $ip;
			$session->data($data)->add();
			if($uid === -1)
			{
				$this->log->log("新遊客{$ip}訪問",$text,'visit');
			}else{
				$this->log->log("新用戶({$uid})訪問",$text,'visit');
			}
		}
		//echo $sid;
		return;
	}
}